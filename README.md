## Installation steps

### Step 1:
git clone https://gitlab.com/karthix/CMS.git

### Step 2:
cd CMS

### Step 3:
bundle install

### Step 4:
rake db:migrate

### Step 5:
rails server

### Step 6:
check http://localhost:3000 in your browser
here is heroku link you can check app in live

spritle-cms.herokuapp.com
## simple web based contact management system
#### some detail description given below

* First i created a controller  contacts and then i generated a model for contacts.

* Then i created a methods index,new,edit,show inside the conroller.

* Rendering a partial form  to move the new  section in seperate file.


* For adding one to many address realtionship i generate  a second model addr with column address and country.

* For making associate relationship between my models i use has_many realtionship in contacts and belongs_to relationship in addr model.

* Adding routes and generating controller for my addr.

* Then i used bootstrap gem to make app pretty good to see.

#### (Note: this is just an contact management application)

class AddrsController < ApplicationController
	def create
    @contact = Contact.find(params[:contact_id])
    @addr = @contact.addrs.create(addrs_params)
    redirect_to contact_path(@contact)
  end
 
  private
    def addrs_params
      params.require(:addr).permit(:address, :country)
    end
end

class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.text :address
      t.string :country
      t.string :phone

      t.timestamps null: false
    end
  end
end

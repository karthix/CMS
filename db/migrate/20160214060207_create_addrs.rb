class CreateAddrs < ActiveRecord::Migration
  def change
    create_table :addrs do |t|
      t.text :address
      t.string :country

      t.timestamps null: false
    end
  end
end

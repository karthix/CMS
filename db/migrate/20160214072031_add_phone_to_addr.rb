class AddPhoneToAddr < ActiveRecord::Migration
  def change
    add_column :addrs, :phone, :string
  end
end

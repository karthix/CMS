class AddContactIdToAddr < ActiveRecord::Migration
  def change
    add_column :addrs, :contact_id, :integer
  end
end
